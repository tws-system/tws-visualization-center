package cn.thoughtworks.school.visualization.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@FeignClient(name = "redash-center",
        url = "https://redash.school.thoughtworks.cn")
@Service
public interface RedashCenterFeign {

    @PostMapping(value = "api/queries", headers = {"Accept=application/json, text/plain, */*", "Content-Type=application/json;charset=UTF-8"})
    Map createQuery(@RequestBody Map map, @RequestParam("api_key") String api_key);

    @PostMapping(value = "api/query_results", headers = {"Accept=application/json, text/plain, */*", "Content-Type=application/json;charset=UTF-8"})
    Map findQueryResults(@RequestBody Map map, @RequestParam("api_key") String api_key);

    @GetMapping(value = "api/jobs/{id}", headers = {"Accept=application/json, text/plain, */*", "Content-Type=application/json;charset=UTF-8"})
    Map findJobsById(@PathVariable(value="id") String id, @RequestParam("api_key") String api_key);

    @GetMapping(value = "api/query_results/{id}", headers = {"Accept=application/json, text/plain, */*", "Content-Type=application/json;charset=UTF-8"})
    Map findQueryResultsById(@PathVariable(value="id") int id, @RequestParam("api_key") String api_key);


}
