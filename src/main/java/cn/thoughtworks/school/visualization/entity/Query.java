package cn.thoughtworks.school.visualization.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "query")
@ToString
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Query {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long queryId;
    private Long programId;
    private String query;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime = new Date();
    private String type;

    public static Query build(Long programId, String type, String id,String sql) {
        return Query.builder()
            .queryId(Long.parseLong(id))
            .programId(programId)
            .createTime(new Date())
            .type(type)
            .query(sql)
            .build();
    }

    public void replaceTutorId(Long tutorId) {
        this.query = this.query.replace("TUTOR_ID", tutorId+"");
    }

    public void replaceAssignmentId(Long assignmentId) {
        this.query = this.query.replace("ASSIGNMENT_ID", assignmentId+"");
    }

    public void replaceTagId(Long tagId) {
        this.query = this.query.replace("TAG_ID", tagId+"");
    }
}
