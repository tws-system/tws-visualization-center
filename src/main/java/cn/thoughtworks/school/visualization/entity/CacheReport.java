package cn.thoughtworks.school.visualization.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class CacheReport {
    private String key;
    private Long programId;
    private Long tutorId;
    private List data;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date refreshTime;

    public static CacheReport build(List data, Long programId, Long tutorId, String type) {
        return CacheReport.builder()
            .data(data)
            .key(type)
            .programId(programId)
            .tutorId(tutorId)
            .refreshTime(new Date())
            .build();
    }

    public void update(List data) {
        this.data = data;
        this.refreshTime = new Date();
    }
}
