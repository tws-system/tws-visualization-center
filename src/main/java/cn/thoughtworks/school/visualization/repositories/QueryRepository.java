package cn.thoughtworks.school.visualization.repositories;

import cn.thoughtworks.school.visualization.entity.Query;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QueryRepository extends JpaRepository<Query, Long> {
    Query findByProgramIdAndType(Long programId, String type);
}
