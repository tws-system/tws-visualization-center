package cn.thoughtworks.school.visualization.controller;

import cn.thoughtworks.school.visualization.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class QueryController {
    @Autowired
    private QueryService queryService;

    @GetMapping("/queries/types/{type}/programs/{programId}/tutors/{tutorId}")
    public ResponseEntity findAssignmentsStudentsStatus(@PathVariable Long programId,
                                                        @PathVariable Long tutorId,
                                                        @PathVariable String type,
                                                        @RequestParam(value = "cache", defaultValue = "false", required=false) boolean cache) {
        return ResponseEntity.ok(queryService.findAssignmentsStudentsStatusData(programId, tutorId, type, cache));
    }

    @GetMapping("/queries/types/{type}/programs/{programId}/tutors/{tutorId}/report")
    public ResponseEntity findAssignmentsStudentsStatusReport(@PathVariable Long programId,
                                                        @PathVariable Long tutorId,
                                                        @PathVariable String type,
                                                        @RequestParam(value = "cache", defaultValue = "false", required=false) boolean cache) {
        return ResponseEntity.ok(queryService.findAssignmentsStudentsStatus(programId, tutorId, type, cache));
    }

    @GetMapping("/queries/types/{type}/programs/{programId}/assignments/{assignmentId}/tutors/{tutorId}")
    public ResponseEntity findAssignmentsStatusByAssignment(@PathVariable Long programId,
                                                            @PathVariable Long tutorId,
                                                            @PathVariable Long assignmentId,
                                                            @PathVariable String type,
                                                            @RequestParam(value = "cache", defaultValue = "false", required=false) boolean cache) {
        return ResponseEntity.ok(queryService.findAssignmentsStudentsStatusByAssignment(programId, assignmentId, tutorId, type, cache));
    }

    @GetMapping("/queries/types/{type}/programs/{programId}/tags/{tagId}/assignments/{assignmentId}/tutors/{tutorId}")
    public ResponseEntity findAssignmentsStatusByTag(@PathVariable Long programId,
                                                     @PathVariable Long tutorId,
                                                     @PathVariable Long tagId,
                                                     @PathVariable Long assignmentId,
                                                     @PathVariable String type,
                                                     @RequestParam(value = "cache", defaultValue = "false", required=false) boolean cache) {
        return ResponseEntity.ok(queryService.findAssignmentsStudentsStatusByTag(programId, tagId, assignmentId, tutorId, type,cache));
    }

}
