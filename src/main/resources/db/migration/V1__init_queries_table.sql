CREATE TABLE `query` (
  `id`           INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `queryId`      INT(11)                      DEFAULT NULL,
  `programId`    INT(11)                      DEFAULT NULL,
  `query`        TEXT                         DEFAULT NULL,
  `createTime`   TIMESTAMP                    DEFAULT CURRENT_TIMESTAMP,
  `type`         VARCHAR(225)                 DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
