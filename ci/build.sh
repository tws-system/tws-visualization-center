#!/usr/bin/env sh

set -e

ci_path=ci
jar_path=build/libs
dest_path=build-api

./gradlew build -x test
mkdir -p $dest_path
cp $jar_path/visualization-0.0.1-SNAPSHOT.jar $dest_path/app.jar

cp $ci_path/Dockerfile $dest_path

